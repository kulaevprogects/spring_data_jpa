package com.kulaev.spring.springboot2.spring_data_jpa.service;


import com.kulaev.spring.springboot2.spring_data_jpa.Entity.Employee;
import com.kulaev.spring.springboot2.spring_data_jpa.dao.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImplementation implements EmployeeService{
    @Autowired
    private EmployeeRepository employeeRepository;
    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public void saveEmployee(Employee employee) {employeeRepository.save(employee);
    }

    @Override
    public Employee getEmployee(int id) {
        Employee employee1 = null;
        Optional<Employee> employee =employeeRepository.findById(id);
        if (employee.isPresent()){
            employee1 = employee.get();
            return  employee1;
        }
        return employee1;
    }

    @Override
    public void deleteEmployee(int id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public List<Employee> findAllByName(String name) {
         return employeeRepository.findAllByName(name);
    }
}
