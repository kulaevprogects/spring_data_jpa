package com.kulaev.spring.springboot2.spring_data_jpa.service;


import com.kulaev.spring.springboot2.spring_data_jpa.Entity.Employee;

import java.util.List;

public interface EmployeeService {
    public List<Employee> getAllEmployees();
    public void saveEmployee(Employee employee);
    public Employee getEmployee(int id);
    public void deleteEmployee(int id);
    public List<Employee> findAllByName(String name);
}
