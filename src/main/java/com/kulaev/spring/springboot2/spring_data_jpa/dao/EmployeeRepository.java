package com.kulaev.spring.springboot2.spring_data_jpa.dao;




import com.kulaev.spring.springboot2.spring_data_jpa.Entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    public List<Employee> findAllByName(String name);
}
