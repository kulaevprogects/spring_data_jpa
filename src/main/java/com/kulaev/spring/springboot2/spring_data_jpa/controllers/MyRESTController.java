package com.kulaev.spring.springboot2.spring_data_jpa.controllers;



import com.kulaev.spring.springboot2.spring_data_jpa.Entity.Employee;
import com.kulaev.spring.springboot2.spring_data_jpa.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MyRESTController {
    @Autowired
    private EmployeeService employeeService;
    @GetMapping("/employees")
    @CrossOrigin
    public List<Employee> showAllEmployees(){
        List<Employee> employees = employeeService.getAllEmployees();
        return employees;
    }
    @GetMapping("/employees/{id}")
    @CrossOrigin
    private Employee getEmployee(@PathVariable int id){
        Employee employee = employeeService.getEmployee(id);
            return employee;
    }
    @PostMapping("/employees")
    @CrossOrigin
    public Employee addNewEmloyee(@RequestBody Employee employee){
        employeeService.saveEmployee(employee);
        return employee;
    }
    @PutMapping("/employees")
    @CrossOrigin
    public Employee updateEmployee(@RequestBody Employee employee){
        employeeService.saveEmployee(employee);
        return  employee;
    }
    @DeleteMapping("/employees/{id}")
    @CrossOrigin
    public String deleteEmployee(@PathVariable int id){
        Employee employee =employeeService.getEmployee(id);
        employeeService.deleteEmployee(id);
        return "Employee with ID = "+id+" was deleted";
    }
    @GetMapping("/employees/name/{name}")
    @CrossOrigin
    public List<Employee> showAllEmployeesByName(@PathVariable String name){
        List<Employee> employees = employeeService.findAllByName(name);
        return employees;
    }
}